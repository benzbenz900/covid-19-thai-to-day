import colors from 'vuetify/es5/util/colors'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Covid 19 Thai Today',
    title: 'โควิด-19 วันนี้',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Covid 19 Thai Today' },
      { name: 'format-detection', content: 'telephone=no' },
      { property: 'og:url', content: 'https://covid19thaitoday.web.app/' },
      { property: 'og:type', content: 'website' },
      { property: 'og:title', content: 'โควิด-19 วันนี้ - Covid 19 Thai Today' },
      { property: 'og:description', content: 'รายงานสถานการณ์ โควิด-19 วันนี้' },
      { property: 'og:image', content: 'https://covid19thaitoday.web.app/ss.jpg' },
      { name: 'google-site-verification', content: '6kaorUi6CAEsLDv9K7FhWXii1Wjp7dkbH4pLDCV2W-c' }
    ],
    link: [
      { rel: 'icon', type: 'image/svg', href: '/covid-19.svg' }
    ],
    script: [
      { src: '/__/firebase/8.8.0/firebase-app.js' },
      { src: '/__/firebase/8.8.0/firebase-analytics.js' },
      { src: '/__/firebase/init.js' },
    ],
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/google-analytics'
  ],
  googleAnalytics: {
    id: 'G-XC2D0SBLQK'
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  generate: {
    dir: '../public',
    fallback: '200.html'
  },
  build: {
  },
}
